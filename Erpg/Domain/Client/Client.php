<?php

namespace Erpg\Domain\Client;

use Erpg\Domain\Account\AccountCollection;
use Erpg\Domain\Address\AddressCollection;

class Client
{
    private $name;
    private $lastname;
    private $addresses;
    private $accounts;
    private $clientNumber;
    private $disabled;
    private $blocked;
    private $maxRisk;
    private $soonPaid;
    private $invoicingType;
    private $payment_terms;

//    public $tariff;
//    #@(type=many2many;map=discount_tariff_version;inverse=clients;tab=3)
//    protected $versions;
//    #@(type=one2many;map=price_rule;inverse=client;widget=lines;tab=3)
//    public $price_rules;
//    #@(type=one2many;map=pack_price_rule;inverse=client;widget=lines;tab=3)
//    public $pack_price_rules;
//    #@(type=many2one;map=shipping_service;inverse=client;tab=1)
//    public $shipping_service;
//    #@(type=boolean;tab=1;disabled=true)
//    public $free_shipping;

    public function __construct(
        $name,
        $lastname,
        ?AddressCollection $addresses,
        ?AccountCollection $accounts
    ) {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->addresses = $addresses;
        $this->accounts = $accounts;
        $this->clientNumber;
        $this->disabled = false;
        $this->blocked = false;
        $this->maxRisk = 0;
        $this->soonPaid = 0;
        $this->invoicingType;
        $this->payment_terms;
    }
}
