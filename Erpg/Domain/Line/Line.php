<?php

namespace Erpg\Domain\Line;

class Line
{
    public $id;
    public $variant;
    public $quantity;
    public $unitPrice;
    public $discount;
    public $secondDiscount;
    public $tax;
    public $status;
    public $total;
}
