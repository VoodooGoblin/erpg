<?php

namespace Erpg\Domain\Line;

use Erpg\Domain\Line\Exception\InvalidLineStatus;
use Erpg\Domain\Line\Exception\InvalidLineStatusChange;

class LineStatus
{
    const CANCELED = 'canceled';
    const DRAFT = 'draft';
    const CONFIRMED = 'confirmed';
    const NON_STOCK = 'non stock';
    const WAITING = 'waiting';
    const RESERVED = 'reserved';
    const ON_DELIVERY_NOTE = 'on delivery note';
    //Todo otra manera de hacer esto
    const VALID_STATUSES = array(
        self::CANCELED,
        self::DRAFT,
        self::CONFIRMED,
        self::WAITING,
        self::RESERVED,
        self::ON_DELIVERY_NOTE,
    );

    private $status;

    public function __construct($status)
    {
        if (in_array($status, self::VALID_STATUSES)) {
            $this->status = $status;
        }
        throw new InvalidLineStatus();
    }

    public function draft()
    {
        if (self::CANCELED == $this->status) {
            return new self(self::DRAFT);
        }
        throw new InvalidLineStatusChange();
    }

    public function confirm()
    {
        if (self::DRAFT == $this->status) {
            return new self(self::CONFIRMED);
        }
        throw new InvalidLineStatusChange();
    }

    public function waiting()
    {
        if (
            self::NON_STOCK == $this->status ||
            self::RESERVED == $this->status
        ) {
            return new self(self::WAITING);
        }
        throw new InvalidLineStatusChange();
    }

    public function reserve()
    {
        if (self::WAITING == $this->status) {
            return new self(self::RESERVED);
        }
        throw new InvalidLineStatusChange();
    }

    public function delivery()
    {
        if (self::RESERVED == $this->status) {
            return new self(self::ON_DELIVERY_NOTE);
        }
        throw new InvalidLineStatusChange();
    }

    public function cancel()
    {
        if (self::ON_DELIVERY_NOTE != $this->status) {
            return new self(self::CANCELED);
        }
        throw new InvalidLineStatusChange();
    }
}
