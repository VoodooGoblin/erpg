<?php

namespace Erpg\Domain\Line\Exception;

//Todo Crear Excepcion generica ERPG_Exception
class InvalidLineStatusChange extends \Exception
{
    const MESSAGE = 'Can not be changed from the current status to the new one';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
