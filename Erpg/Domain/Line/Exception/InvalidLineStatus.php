<?php

namespace Erpg\Domain\Line\Exception;

//Todo Crear Excepcion generica ERPG_Exception
class InvalidLineStatus extends \Exception
{
    const MESSAGE = 'Status given is not a valid one';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
