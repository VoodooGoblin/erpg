<?php

namespace Erpg\Domain\Line;

use Erpg\Domain\Base\CollectionInterface;
use Erpg\Domain\Base\CollectionTrait;
use Erpg\Domain\Base\CountableTrait;
use Erpg\Domain\Base\IteratorTrait;

class LineCollection implements CollectionInterface
{
    use
        CountableTrait,
        IteratorTrait,
        CollectionTrait {
            add as private traitAdd;
        }

    public function add(Line $line)
    {
        $this->traitAdd($line);
    }

    public function totalPriceAmount()
    {
        $amount = 0;
        foreach ($this->items as $line) {
            $amount += $line->totalAmount();
        }

        return $amount;
    }
}
