<?php

namespace Erpg\Domain\Payment;

use Erpg\Domain\Payment\Exception\InvalidPaymentTerm;

class PaymentTerm
{
    const CASH_DOWN = 'cash down';
    const FIFTEEN_DAYS = '15 days';
    const FIFTEEN_THIRTY_DAYS = '15-30 days';
    const THIRTY_DAYS = '30 days';
    const THIRTY_SIXTY_DAYS = '30-60 days';
    const SIXTY_DAYS = '60 days';
    const THIRTY_SIXTY_NINETY_DAYS = '30-60-90 days';
    const VALID_TERMS = array(
        self::CASH_DOWN,
        self::FIFTEEN_DAYS,
        self::FIFTEEN_DAYS,
        self::FIFTEEN_THIRTY_DAYS,
        self::THIRTY_DAYS,
        self::THIRTY_SIXTY_DAYS,
        self::SIXTY_DAYS,
        self::THIRTY_SIXTY_NINETY_DAYS,
    );

    private $term;

    public function __construct($term)
    {
        if (in_array($term, self::VALID_TERMS)) {
            $this->term = $term;
        }
        throw new InvalidPaymentTerm();
    }
}
