<?php

namespace Erpg\Domain\Payment\Exception;

class InvalidPaymentTerm extends \Exception
{
    const MESSAGE = 'Term given is not a valid one';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
