<?php

namespace Erpg\Domain\User;

use Ramsey\Uuid\Uuid;

class UserId
{
    private $id;

    public function __construct()
    {
        $this->id = Uuid::uuid4();
    }
}
