<?php

namespace Erpg\Domain\User;

class User
{
    public $userId;
    public $username;
    public $email;
    private $password;

    public function __construct(
        $username,
        $email,
        $password
    ) {
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->userId = new UserId();
    }
}
