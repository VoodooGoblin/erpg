<?php

namespace Erpg\Domain\User;

interface UserRepositoryInterface
{
    public function find(UserId $userId): User;

    public function save(User $user);

    public function delete(User $user);
}
