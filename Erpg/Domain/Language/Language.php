<?php

namespace Erpg\Domain\Language;

class Language
{
    private $ISOName;
    private $ISOCode1;
    private $ISOCode2;

    public function getISOName()
    {
        return $this->ISOName;
    }

    public function getISOCode1()
    {
        return $this->ISOCode1;
    }

    public function getISOCode2()
    {
        return $this->ISOCode2;
    }

    public function equals(self $language)
    {
        return $this->ISOName == $language->getISOName()
            && $this->ISOCode1 == $language->getISOCode1()
            && $this->ISOCode2 == $language->getISOCode2();
    }
}
