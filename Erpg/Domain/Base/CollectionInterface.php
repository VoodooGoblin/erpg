<?php

namespace Erpg\Domain\Base;

interface CollectionInterface extends \Iterator, \Countable
{
}
