<?php
/**
 * Created by PhpStorm.
 * User: ninef
 * Date: 18/03/19
 * Time: 19:58.
 */

namespace Erpg\Domain\Base;

trait CollectionTrait
{
    private $items;
    private $current;

    public function __construct()
    {
        $this->reset();
    }

    private function reset(): void
    {
        $this->items = array();
        $this->rewind();
    }

    public function unshift($value): void
    {
        array_unshift($this->items, $value);
        $this->rewind();
    }

    public function add($value): void
    {
        $this->items[] = $value;
        $this->rewind();
    }

    public function addOnly($value): void
    {
        if (false === $this->indexOf($value)) {
            $this->add($value);
        }
        $this->rewind();
    }

    public function addCollection(self $other): void
    {
        foreach ($other as $item) {
            $this->add($item);
        }
    }

    public function addArray($arr, $only = false)
    {
        foreach ($arr as $item) {
            if ($only) {
                $this->addOnly($item);
                continue;
            }
            $this->add($item);
        }
    }

    public function set(array $arr)
    {
        $this->items = $arr;
        $this->rewind();
    }

//    public function get($index, $default = null)
//    {
//        $index = (int) $index;
//        if ($index < 0 && $this->count() >= abs($index)) {
//            $index += $this->count();
//        }
//        if (array_key_exists($index, $this->items)) {
//            return $this->items[$index];
//        }
//
//        return $default;
//    }

    public function getArray()
    {
        return $this->items;
    }

    public function indexOf($searchedValue)
    {
        foreach ($this->items as $key => $value) {
            if ($value === $searchedValue) {
                return $key;
            }
        }

        return false;
    }

    public function indexesOf($searchedValue)
    {
        $keys = array();
        foreach ($this->items as $key => $value) {
            if ($value === $searchedValue) {
                $keys[] = $key;
            }
        }

        return empty($keys) ? false : $keys;
    }

    public function remove($valueToRemove)
    {
        $deleteKeys = $this->indexesOf($valueToRemove);
        for ($i = count($deleteKeys) - 1; $i >= 0; --$i) {
            $this->removeByIndex($deleteKeys[$i]);
        }
    }

    public function removeAll(array $items)
    {
        foreach ($items as $item) {
            $this->remove($item);
        }
    }

    public function removeByIndex($index)
    {
        if (is_integer($index) && array_key_exists($index, $this->items)) {
            array_splice($this->items, $index, 1);
        }
    }

    public function copy()
    {
        $col = new static();
        $col->addCollection($this);

        return $col;
    }

    public function isEmpty()
    {
        return empty($this->items);
    }

    public function map(callable $func)
    {
        $args = func_get_args();
        array_splice($args, 0, 1, array(0, 0));
        foreach ($this->items as $key => $item) {
            $args[0] = $key;
            $args[1] = $item;
            $this->items[$key] = call_user_func_array($func, $args);
        }
    }

    public function matchFirst(callable $func)
    {
        $args = func_get_args();
        array_splice($args, 0, 1, array(0, 0));
        foreach ($this->items as $key => $item) {
            $args[0] = $key;
            $args[1] = $item;
            $response = call_user_func_array($func, $args);
            if ($response) {
                return $item;
            }
        }

        return false;
    }

    public function match(callable $func)
    {
        $matches = new static();
        $args = func_get_args();
        array_splice($args, 0, 1, array(0, 0));
        foreach ($this->items as $key => $item) {
            $args[0] = $key;
            $args[1] = $item;
            $response = call_user_func_array($func, $args);
            if ($response) {
                $matches->add($item);
            }
        }

        return $matches;
    }

    public function clear()
    {
        $this->reset();
    }

    public function __toString()
    {
        return implode(', ', $this->items);
    }
}
