<?php

namespace Erpg\Domain\Base;

trait CountableTrait
{
    private $items;

    public function count()
    {
        return count($this->items);
    }
}
