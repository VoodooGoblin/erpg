<?php

namespace Erpg\Domain\Event;

interface EventInterface
{
    public function getName();
}
