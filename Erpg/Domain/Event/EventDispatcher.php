<?php

namespace Erpg\Domain\Event;

class EventDispatcher
{
    private static $instance;
    private $subscribers;

    private function __construct()
    {
        $this->subscribers = new SubscriberCollection();
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function addSubscriber(SubscriberInterface $subscriber)
    {
        $this->subscribers->add($subscriber);
    }

    public function notifyEvent(EventInterface $event)
    {
        $this->subscribers->notifyEvent($event);
    }
}
