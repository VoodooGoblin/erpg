<?php

namespace Erpg\Domain\Event;

interface SubscriberInterface
{
    public function isSubscribedTo(EventInterface $event);
}
