<?php

namespace Erpg\Domain\Event;

class SubscriberCollection implements \Iterator, \Countable
{
    private $subscribers;
    private $position;

    public function __construct()
    {
        $this->position = 0;
    }

    public function add(SubscriberInterface $subscriber)
    {
        $this->subscribers[] = $subscriber;
    }

    public function getSubscriberByEvent(EventInterface $event)
    {
        return array_filter(
            $this->subscribers,
            function ($subscriber) use ($event) {
                return $subscriber->isSubscribedTo($event);
            });
    }

    public function notifyEvent(EventInterface $event)
    {
        foreach ($this->subscribers as $subscriber) {
            if ($subscriber->isSubscribedTo($event)) {
                $subscriber->handle($event);
            }
        }
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->subscribers[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->subscribers[$this->position]);
    }

    public function count()
    {
        return count($this->subscribers);
    }
}
