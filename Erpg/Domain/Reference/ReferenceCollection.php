<?php

namespace Erpg\Domain\Reference;

use Erpg\Domain\Base\CollectionInterface;
use Erpg\Domain\Base\CollectionTrait;
use Erpg\Domain\Base\CountableTrait;
use Erpg\Domain\Base\IteratorTrait;

class ReferenceCollection implements CollectionInterface
{
    use
        CountableTrait,
        IteratorTrait,
        CollectionTrait {
        add as private traitAdd;
    }

    public function add(Reference $reference)
    {
        $this->traitAdd($reference);
    }

    public function contains(Reference $reference): bool
    {
        foreach ($this->items as $ref) {
            if ($reference->equals($ref)) {
                return true;
            }
        }

        return false;
    }
}
