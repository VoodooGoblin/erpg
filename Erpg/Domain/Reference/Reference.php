<?php

namespace Erpg\Domain\Reference;

class Reference
{
    private $code;

    public function getCode()
    {
        return $this->code;
    }

    public function equals(self $reference)
    {
        return $this->code === $reference->getCode();
    }
}
