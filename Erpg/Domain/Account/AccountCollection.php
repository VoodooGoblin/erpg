<?php

namespace Erpg\Domain\Account;

use Erpg\Domain\Base\CollectionInterface;
use Erpg\Domain\Base\CollectionTrait;
use Erpg\Domain\Base\CountableTrait;
use Erpg\Domain\Base\IteratorTrait;

class AccountCollection implements CollectionInterface
{
    use
        CountableTrait,
        IteratorTrait,
        CollectionTrait {
        add as private traitAdd;
    }

    public function add(Account $account)
    {
        $this->traitAdd($account);
    }
}
