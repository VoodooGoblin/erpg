<?php

namespace Erpg\Domain\Account;

const ACCOUNT_TYPE_CLIENT = 'client';
    const ACCOUNT_TYPE_SUPPLIER = 'supplier';
    const ACCOUNT_TYPE_SOON_PAYMENT = 'advanced payment';
    const ACCOUNT_TYPE_ADVANCED_PAYMENT = 'advanced payment';

class Account
{
    private $accountNumber;
    private $type;
    private $active;
}
