<?php

namespace Erpg\Domain\Address;

use Erpg\Domain\Base\CollectionInterface;
use Erpg\Domain\Base\CollectionTrait;
use Erpg\Domain\Base\CountableTrait;
use Erpg\Domain\Base\IteratorTrait;

class AddressCollection implements CollectionInterface
{
    use
        CountableTrait,
        IteratorTrait,
        CollectionTrait {
        add as private traitAdd;
    }

    public function add(Address $address)
    {
        $this->traitAdd($address);
    }
}
