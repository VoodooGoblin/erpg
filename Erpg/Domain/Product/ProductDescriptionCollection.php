<?php

namespace Erpg\Domain\Product;

use Erpg\Domain\Language\Language;

class ProductDescriptionCollection implements \Iterator, \Countable
{
    private $descriptions;
    private $position;

    public function __construct()
    {
        $this->position = 0;
    }

    public function getDescriptionByLanguage(Language $language): ?ProductDescription
    {
        foreach ($this->descriptions as $description) {
            if ($language->equals($description->getLanguage())) {
                return $description;
            }
        }

        return null;
    }

    public function add(ProductDescription $description)
    {
        $this->descriptions[] = $description;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->descriptions[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->descriptions[$this->position]);
    }

    public function count()
    {
        return count($this->descriptions);
    }
}
