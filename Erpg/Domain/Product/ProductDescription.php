<?php

namespace Erpg\Domain\Product;

use Erpg\Domain\Language\Language;

class ProductDescription
{
    private $title;
    private $description;
    private $language;

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }
}
