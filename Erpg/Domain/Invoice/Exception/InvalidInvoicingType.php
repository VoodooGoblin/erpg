<?php

namespace Erpg\Domain\Invoice\Exception;

class InvalidInvoicingType extends \Exception
{
    const MESSAGE = 'Type given is not a valid one';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
