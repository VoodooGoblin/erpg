<?php

namespace Erpg\Domain\Invoice;

use Erpg\Domain\Invoice\Exception\InvalidInvoicingType;

class InvoicingType
{
    const AT_MOMENT = 'at moment';
    const FIFTEEN_DAYS = 'fifteen days';
    const ONE_MONTH = 'one month';
    //Todo otra manera de hacer esto
    const VALID_TYPES = array(
        self::AT_MOMENT,
        self::FIFTEEN_DAYS,
        self::ONE_MONTH,
    );

    private $type;

    public function __construct($type)
    {
        if (in_array($type, self::VALID_TYPES)) {
            $this->type = $type;
        }
        throw new InvalidInvoicingType();
    }
}
