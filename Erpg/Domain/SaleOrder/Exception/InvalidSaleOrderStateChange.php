<?php

namespace Erpg\Domain\SaleOrder\Exception;

//Todo Crear Excepcion generica ERPG_Exception
class InvalidSaleOrderStatusChange extends \Exception
{
    private const MESSAGE = 'Can not be changed from the current status to the new one';

    public function __construct()
    {
        parent::__construct(self::MESSAGE);
    }
}
