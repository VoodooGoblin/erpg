<?php

namespace Erpg\Domain\SaleOrder;

class ReservedState
{
    public function isReserved()
    {
        return true;
    }

    public function canBeDelivered()
    {
        return true;
    }

    public function canBeCanceled()
    {
        return true;
    }
}
