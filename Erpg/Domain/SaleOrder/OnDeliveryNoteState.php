<?php

namespace Erpg\Domain\SaleOrder;

class OnDeliveryNoteState extends SaleOrderState
{
    public function isDelivered()
    {
        return true;
    }
}
