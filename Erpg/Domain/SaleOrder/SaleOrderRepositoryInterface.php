<?php

namespace Erpg\Domain\SaleOrder;

interface SaleOrderRepositoryInterface
{
//    public function find(SaleOrderId $saleOrderId);

    public function save(SaleOrder $saleOrder);

    public function delete(SaleOrder $saleOrder);
}
