<?php

namespace Erpg\Domain\SaleOrder;

class CanceledState extends SaleOrderState
{
    public function isCanceled()
    {
        return true;
    }

    public function canBeDraft()
    {
        return true;
    }
}
