<?php

namespace Erpg\Domain\SaleOrder;

use Erpg\Domain\Client\Client;
use Erpg\Domain\Line\Line;
use Erpg\Domain\Line\LineCollection;
use Erpg\Domain\SaleOrder\Exception\InvalidSaleOrderStatusChange;

class SaleOrder
{
    private $id;
    private $draftReference;
    private $saleReference;
    private $state;
    private $client;
    private $lines;

    public function __construct(
        Client $client
    ) {
        $this->client = $client;
        $this->state = new DraftState();
        $this->lines = new LineCollection();
    }

    public function createLine($lineData)
    {
        //Todo Logica de la linea
//        $line = new Line($lineData);
//        $this->lines->add($line);
    }

    public function confirm()
    {
        if (!$this->state->canBeConfirmed()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new ConfirmedState();
    }

    public function reserve()
    {
        if (!$this->state->canBeReserved()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new ReservedState();
    }

    public function cancel()
    {
        if (!$this->state->canBeCanceled()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new CanceledState();
    }

    public function draft()
    {
        if (!$this->state->canBeDraft()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new DraftState();
    }

    public function request()
    {
        if (!$this->state->canBeWaiting()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new WaitingState();
    }

    public function deliver()
    {
        if (!$this->state->canBeWaiting()) {
            throw new InvalidSaleOrderStatusChange();
        }
        $this->state = new OnDeliveryNoteState();
    }

    public function getState(): SaleOrderState
    {
        return $this->state;
    }
}
