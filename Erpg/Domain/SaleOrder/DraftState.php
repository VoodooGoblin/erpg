<?php

namespace Erpg\Domain\SaleOrder;

class DraftState extends SaleOrderState
{
    public function isDraft()
    {
        return true;
    }

    public function canBeConfirmed()
    {
        return true;
    }

    public function canBeCanceled()
    {
        return true;
    }
}
