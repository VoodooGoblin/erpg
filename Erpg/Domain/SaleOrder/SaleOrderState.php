<?php

namespace Erpg\Domain\SaleOrder;

abstract class SaleOrderState
{
    public function canBeDraft()
    {
        return false;
    }

    public function canBeConfirmed()
    {
        return false;
    }

    public function canBeWaiting()
    {
        return false;
    }

    public function canBeReserved()
    {
        return false;
    }

    public function canBeDelivered()
    {
        return false;
    }

    public function canBeCanceled()
    {
        return false;
    }

    public function isDraft()
    {
        return false;
    }

    public function isConfirmed()
    {
        return false;
    }

    public function isWaiting()
    {
        return false;
    }

    public function isReserved()
    {
        return false;
    }

    public function isDelivered()
    {
        return false;
    }

    public function isCanceled()
    {
        return false;
    }
}
