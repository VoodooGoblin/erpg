<?php

namespace Erpg\Domain\SaleOrder;

class ConfirmedState extends SaleOrderState
{
    public function isConfirmed()
    {
        return true;
    }

    public function canBeReserved()
    {
        return true;
    }

    public function canBeWaiting()
    {
        return true;
    }

    public function canBeCancel()
    {
        return true;
    }
}
