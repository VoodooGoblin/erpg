<?php

namespace Erpg\Domain\SaleOrder;

class WaitingState extends SaleOrderState
{
    public function isWaiting()
    {
        return true;
    }

    public function canBeCancel()
    {
        return true;
    }

    public function canReserved()
    {
        return true;
    }
}
