<?php

namespace Erpg\Domain\Variant;

class WarehouseVariantInformation
{
    public $weight;
    public $volume;
    public $minimumStock;
    public $amountToRestock;
}
