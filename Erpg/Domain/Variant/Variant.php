<?php

namespace Erpg\Domain\Variant;

use Erpg\Domain\Reference\ReferenceCollection;

class Variant
{
    private $shortName;
    private $brand;
    private $eans;
    private $season;
    private $references;
    private $category;
    private $saleDisabled;

    public function getReferences(): ReferenceCollection
    {
        return $this->references;
    }
}
