<?php

namespace Erpg\Domain\Variant;

use Erpg\Domain\Base\CollectionInterface;
use Erpg\Domain\Base\CollectionTrait;
use Erpg\Domain\Base\CountableTrait;
use Erpg\Domain\Base\IteratorTrait;
use Erpg\Domain\Reference\Reference;

class VariantCollection implements CollectionInterface
{
    use
        CountableTrait,
        IteratorTrait,
        CollectionTrait {
        add as private traitAdd;
    }

    public function add(Variant $variant)
    {
        $this->traitAdd($variant);
    }

    public function getByReference(Reference $reference): ?Variant
    {
        //Reference y brand???
        foreach ($this->items as $variant) {
            if ($variant->contains($reference)) {
                return $variant;
            }
        }

        return null;
    }
}
