<?php

namespace Erpg\Application\SaleOrder;

use Erpg\Domain\Client\Client;
use Erpg\Domain\SaleOrder\SaleOrder;
use Erpg\Domain\SaleOrder\SaleOrderRepositoryInterface;

class BetaCreateSaleOrderHandler
{
    private $saleOrderRepository;

    public function __construct(SaleOrderRepositoryInterface $saleOrderRepository)
    {
        $this->saleOrderRepository = $saleOrderRepository;
    }

    public function handle(BetaCreateSaleOrderCommand $command)
    {
        $client = new Client(1, 1, null, null);
        $saleOrder = new SaleOrder($client);
        $this->saleOrderRepository->save($saleOrder);
    }
}
