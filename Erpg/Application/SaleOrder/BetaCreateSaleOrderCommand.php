<?php

namespace Erpg\Application\SaleOrder;

class BetaCreateSaleOrderCommand
{
    private $clientId;

    public function __construct($clientId)
    {
        $this->clientId = $clientId;
    }

    public function clientId()
    {
        return $this->clientId;
    }
}
