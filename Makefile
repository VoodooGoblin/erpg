
PRECOMMIT_FILES := $(shell git --no-pager diff --name-only --diff-filter=ACM HEAD -- | grep .php$$)
DOCKER_COMPOSE := docker-compose -f docker/docker-compose.yml
CS_FIXER := vendor/friendsofphp/php-cs-fixer/php-cs-fixer
PHP_UNIT := ./vendor/bin/phpunit --bootstrap vendor/autoload.php --testdox

.SILENT: cs-fix-all cs-check cs-fix

#Docker-compose

run:
	$(DOCKER_COMPOSE) up
down:
	$(DOCKER_COMPOSE) down
kill:
	$(DOCKER_COMPOSE) kill
recreate:
	$(DOCKER_COMPOSE) create --force-recreate --build

#PHP - Composer

composer:
	$(DOCKER_COMPOSE) exec php composer $(REST)
composer-install:
	$(DOCKER_COMPOSE) exec php composer $(REST) install
composer-update:
	$(DOCKER_COMPOSE) exec php composer $(REST) update

#CS-Fixer

cs-fix-all:
	$(DOCKER_COMPOSE) exec php $(CS_FIXER) fix --config=scripts/.php_cs.dist --using-cache=no
cs-check:
	$(DOCKER_COMPOSE) exec php $(CS_FIXER) fix --config=scripts/.php_cs.dist --dry-run --using-cache=no
cs-fix:
	if [ $(words ${PRECOMMIT_FILES}) -gt 0 ]; then \
			$(DOCKER_COMPOSE) exec php $(CS_FIXER) fix --config=scripts/.php_cs.dist --using-cache=no $(PRECOMMIT_FILES);\
	fi

#PHPUnit:

php-unit:
    ifdef FILE
		$(DOCKER_COMPOSE) exec php $(PHP_UNIT) $(FILE)
    else
		@echo 'Falta archivo o path. ej: --FILE="./Tests/test.php"'
    endif
php-unit-all:
	$(DOCKER_COMPOSE) exec php $(PHP_UNIT) ./Tests

#PHPStan:

php-stan:
	$(DOCKER_COMPOSE) exec php vendor/bin/phpstan analyse Erpg Tests --level max


#Rabbit

rabbit-ui:
	@xdg-open http://localhost:15672

#Nginx

nginx-access-log:
	$(DOCKER_COMPOSE) exec web tail -f /var/log/nginx/access.log
nginx-error-log:
	$(DOCKER_COMPOSE) exec web tail -f /var/log/nginx/error.log