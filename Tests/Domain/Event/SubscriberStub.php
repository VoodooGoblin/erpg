<?php

namespace Test\Domain\Event;

use Erpg\Domain\Event\EventInterface;
use Erpg\Domain\Event\SubscriberInterface;

class SubscriberStub implements SubscriberInterface
{
    public function handle($event)
    {
        return $event->plusOne();
    }

    public function isSubscribedTo(EventInterface $event)
    {
        return $event instanceof EventStub;
    }
}
