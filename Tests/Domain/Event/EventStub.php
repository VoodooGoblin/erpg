<?php

namespace Test\Domain\Event;

use Erpg\Domain\Event\EventInterface;

class EventStub implements EventInterface
{
    private $counter;

    public function __construct($counter)
    {
        $this->counter = $counter;
    }

    public function plusOne()
    {
        ++$this->counter;
    }

    public function getName()
    {
        return self::class;
    }

    public function getCounter()
    {
        return $this->counter;
    }
}
