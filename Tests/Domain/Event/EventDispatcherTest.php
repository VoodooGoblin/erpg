<?php

namespace Test\Domain\Event;

use Erpg\Domain\Event\EventDispatcher;
use PHPUnit\Framework\TestCase;

class EventDispatcherTest extends TestCase
{
    public function testDispatcherCycle()
    {
        $dispatcher = EventDispatcher::getInstance();
        $subscriber = new SubscriberStub();
        $dispatcher->addSubscriber($subscriber);
        $event = new EventStub(0);
        $dispatcher->notifyEvent($event);

        $this->assertEquals(1, $event->getCounter());
    }
}
