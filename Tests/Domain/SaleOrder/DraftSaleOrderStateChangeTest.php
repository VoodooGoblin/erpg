<?php

namespace Test\Domain\SaleOrder;

use Erpg\Domain\Client\Client;
use Erpg\Domain\SaleOrder\CanceledState;
use Erpg\Domain\SaleOrder\ConfirmedState;
use Erpg\Domain\SaleOrder\Exception\InvalidSaleOrderStatusChange;
use Erpg\Domain\SaleOrder\SaleOrder;
use PHPUnit\Framework\TestCase;

class DraftSaleOrderStateChangeTest extends TestCase
{
    /**
     * @var SaleOrder
     */
    private $saleOrder;

    public function setUp(): void
    {
        $this->saleOrder = new SaleOrder(
            new Client(
                null,
                null,
                null,
                null
            )
        );
    }

    public function testDraftSaleOrderBeConfirmedSuccessfully()
    {
        $this->saleOrder->confirm();
        $this->assertInstanceOf(
            ConfirmedState::class,
            $this->saleOrder->getState()
        );
    }

    public function testDraftSaleOrderBeCanceledSuccessfully()
    {
        $this->saleOrder->cancel();
        $this->assertInstanceOf(
            CanceledState::class,
            $this->saleOrder->getState()
        );
    }

    public function testDraftSaleOrderCantBeReserved()
    {
        $this->expectException(InvalidSaleOrderStatusChange::class);
        $this->saleOrder->reserve();
    }

    public function testDraftSaleOrderCantBeSetDraft()
    {
        $this->expectException(InvalidSaleOrderStatusChange::class);
        $this->saleOrder->draft();
    }

    public function testDraftSaleOrderCantBeSetWaiting()
    {
        $this->expectException(InvalidSaleOrderStatusChange::class);
        $this->saleOrder->request();
    }

    public function testDraftSaleOrderCantBeDelivered()
    {
        $this->expectException(InvalidSaleOrderStatusChange::class);
        $this->saleOrder->deliver();
    }
}
